# A Formal Reusable Metamodel for Software Architectures with Composite Components

## Description

The following repository contains a Formal Reusable Metamodel for Software Architectures with Composite Components implemented in Alloy. The metamodel is based of the metamodels in [https://doi.org/10.1016/j.sysarc.2021.102073] and [https://doi.org/10.1016/j.future.2020.02.033] to specify a component-port-connector model’s architectural and behavioural views (Logical and Scenario). The metamodel found here adds a component hierarchy using composite components to create a tree structure for the system model’s architecture. Composite components aggregate groups of components into an abstract component which contains the components within and inherit the internal components ports that communicate with  components outside the aggregated group. 

A message passing communication style is used to represent message passing between components through their ports, and the connectors that connect them, in the Scenario view of the metamodel. Interfaces with operations are realized by the ports of components to be able to create functional contracts between component ports for passing structured messages(operation calls) to each other. Operation calls extend the message passing style by adding the operation and argument fields to call a specific operation of an interface on a port using the arguments as the values for the parameters of the operation. This allows the receiving component to extract the arguments of the message based on the operation and use them to determine and in the components current or next action. 

The Alloy metamodel is specified using a first-order temporal logic on sets and relations. The first-order temporal logic describes how the component model behaves over time based on the logical constraints of the model. The sets and relations of the metamodel are combined to create component and system states.

We include the specification and verification of a [Smart Meter Gateway (SMGW) system](https://www.commoncriteriaportal.org/files/ppfiles/pp0073b_pdf.pdf) for a case study of its process meter data scenario between commodity consumption meters in a Local Meterological Network (LMN) with a Gateway. The Gateway processes the meter data from the LMN and sends it to the appropriate authorized entity in the Wide Area Network (WAN).

## Technologies

This project uses the following tools:
- [Alloy Analyzer 6.1.0](https://alloytools.org/)
- [Java 8 or Above](https://www.java.com/en/) (See link or your local distribution for release)

## Files

All Alloy specification files are within the **Modular-CBSE** folder:
- **cbseFramework.als**: The Alloy formalization of our metamodel enabling the definition of Composite Components in a component-port-connector.
- **SMGW_System.als**: The specification of the Smart Meter Gateway's components, their behaviour and composition.
- **SMGW_Verification.als** and **SMGW_CompositeAssertions.als**: The specification of the SMGW's Atomic and Composite properties and assertions for verification.
- **SMGW_Theme.thm**: A theme file for loading metamodel entities in the Alloy visualizer.

## Usage

Alloy is a self-contained jar file. Once downloaded, Alloy can be run by double-clicking the jar or with the following command:
> java -jar org.alloytools.alloy.dist.jar

The files inside the Modular-CBSE folder can then be opened through the Alloy application using Alloy's menu (File -> Open). Once the files are open in the Alloy application, the Execute menu item can be used to select and analyze any **run** or **check** predicates specified within the files of this repository (Execute -> {command to run}). The "Options" menu allows a modeller to adjust Alloy's settings including the model checkers settings.
