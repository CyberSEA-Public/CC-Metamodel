open SMGW_System

assert WANOnlyReceivesProcessedMeterDataFromLMN {
	always all pmd:ProcessedMeterData,  wan_rcv: CC_WAN.input[].received,  lmn_sent:CC_LMN.output[].sent.arguments[P_RawMeterData] |
				arguments.pmd.P_ExtEntPData  in wan_rcv =>
					once pmd.p_meter_data in lmn_sent
}
check WANOnlyReceivesProcessedMeterDataFromLMN for 1 but 24 Component, 10 Connector, 32 Port, 10 Operation, 7 Parameter,
						16 Variable, 16 Constant, 1 Message,
						4 Int, 14 steps

assert AllMeterDataFromLMNisDeliveredToTheGateway {
	always all md:MeterData | all md_opcalls: arguments.md.P_RawMeterData  | once md in CC_LMN.output[].sent.arguments[P_RawMeterData] and 
							md_opcalls.receivedOn[] => md_opcalls.receivedBy[] in CC_Gateway_TOE.input[]
}
check AllMeterDataFromLMNisDeliveredToTheGateway for 1 but 24 Component, 10 Connector, 32 Port, 10 Operation, 2 Parameter,
						16 Variable, 16 Constant, 1 Message,
						4 Int, 5 steps
