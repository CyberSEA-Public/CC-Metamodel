open SMGW_System

// Individual Properties
// Comms =========================
pred Pt_Comms_ForwardMeterData {
	always {
		let opi = IN_GW_LMN.received, opo = OUT_Comms_TSF.sent {
			all md:opi.arguments[P_RawMeterData] {
				after md in opo.arguments[P_MeterDataComms]
			}
		}
	}
}
pred Pt_Comms_SendProcessedMeterData {
	always {
		let opi = IN_Comms_TSF.received, opo = OUT_GW_WAN.sent {
			all pmd:opi.arguments[P_ProcessedMeterData] {
				after one oc:opo { pmd in oc.arguments[P_ExtEntPData] and pmd.dest in oc.to.user[] }
			}
		}
	}
}

pred Comms_Properties {
	Pt_Comms_ForwardMeterData
	Pt_Comms_SendProcessedMeterData
}

assert Comms_Assert { Comms_Properties }

check Comms_Assert for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 4 Message, 5 steps

// TSF ===========================
pred Pt_TSF_ValidateBeforeProcessing {
	always {
		let opi = IN_TSF_Comms.received, opo = OUT_TSF_SecMod.sent {
			all md:opi.arguments[P_MeterDataComms] {
				after md in opo.arguments[P_MeterAuth]
			}
		}
	}
}

pred Pt_TSF_ProcessValidMeterData { //  P_MeterAuth_reply P_MeterAuth_data
	always {
		let opi = IN_TSF_SecMod.received {
			all o:opi {
				o.arguments[P_MeterAuth_reply] = VALID =>
					AC_Gateway_TSF.processMeterData[o.arguments[P_MeterAuth_data]]

			}
		}
	}
}
pred Pt_TSF_SendProcessedMeterData {
	always {
		let opo = OUT_TSF_Comms.sent {
			all pmd:AC_Gateway_TSF.processed_meter_data {
				eventually pmd in opo.arguments[P_ProcessedMeterData]
			}
		}
	}
}
pred TSF_Properties {
	Pt_TSF_ValidateBeforeProcessing
	Pt_TSF_ProcessValidMeterData
	Pt_TSF_SendProcessedMeterData
}
assert TSF_Assert { TSF_Properties }
check TSF_Assert for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 4 Message, 5 steps

// SecMod ========================
pred Pt_SecMod_Validates {
	always {
		let opi = IN_SecMod_TSF.received, opo = OUT_SecMod_TSF.sent {
			all md:opi.arguments[P_MeterAuth] {
				after md in opo.arguments[P_MeterAuth_data]
			}
		}
	}
}
pred SecMod_Properties {
	Pt_SecMod_Validates
}
assert SecMod_Assert {
	SecMod_Properties
}
check SecMod_Assert for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 4 Message, 5 steps

// CC Gateway TOE ================
pred Pt_GW_AllMeterDataIsProcessedAndSent {
	always {
		let opi = IN_GW_LMN.received, opo = OUT_GW_WAN.sent {
			all md:opi.arguments[P_RawMeterData] {
				after eventually one oc:opo {
					md in oc.arguments[P_ExtEntPData].p_meter_data
					oc.arguments[P_ExtEntPData].dest in oc.to.user[]
				}
			}
		}
	}
}
pred Gateway_Property {
	(Comms_Properties and TSF_Properties and SecMod_Properties and CC_Gateway_TOE.ReliableInternals) => 
		Pt_GW_AllMeterDataIsProcessedAndSent
}
assert Gateway_Assert {
	Gateway_Property
}
check Gateway_Assert for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 6 Message, 14 steps

assert Gateway_SingleProperties {
	Comms_Properties
	TSF_Properties
	SecMod_Properties
}
check Gateway_SingleProperties for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 4 Message, 5 steps

// Whole atomic verification
/*assert Gateway_AC_Assert {
	// Assume all connectors in the Gateway are reliable
	Pt_GW_AllMeterDataIsProcessedAndSent
}
check Gateway_AC_Assert for 1 but exactly 2 AC_Meter, 24 Component, 10 Connector, 32 Port, 10 Operation, 8 Parameter,
						16 Variable, 16 Constant, 6 Message,
						4 Int, 14 steps
*/

// CC LMN ========================
pred Pt_LMN_AllDataGeneratedLeavesLMN {
	always all md:MeterData | md in AC_Meter.mdata =>
						 eventually md in CC_LMN.output[].sent.arguments[P_RawMeterData]
}
check LMN_Properties {
	Pt_LMN_AllDataGeneratedLeavesLMN
} for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 2 Message,
						4 Int, 5 steps

// CC WAN ========================
pred Pt_WAN_AllDataReceivedByAppropriateExtEnt {
	always {
		let opi = IN_Ext_Ent.received {
			all oc:opi {
				oc.arguments[P_ExtEntPData].dest in oc.receivedBy[].user[] => 
					oc.arguments[P_ExtEntPData] in AC_Ext_Ent.p_mdata_storage'
			}
		}
	}
}
check WAN_Properties {
	Pt_WAN_AllDataReceivedByAppropriateExtEnt
} for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 2 Message, 5 steps

// System Meter Data Processing ==
pred Pt_SMGW_ProcessAllMeterData {
	always {
		all md:AC_Meter.mdata {
			eventually md in AC_Ext_Ent.p_mdata_storage.p_meter_data
		}
	}
}
assert SMGW_TotalProcess {
	{
		Pt_LMN_AllDataGeneratedLeavesLMN and
		Pt_GW_AllMeterDataIsProcessedAndSent and
		Pt_WAN_AllDataReceivedByAppropriateExtEnt and
		System.ReliableInternals[]
	} => Pt_SMGW_ProcessAllMeterData
}

check SMGW_TotalProcess for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 3 Message, 6 steps

assert SMGW_AC {
	// gen md (1) -> send md (2) -> (3) forward md (4) -> (5) validate md (6) -> 
	// (7) reply (8) -> (9) process -> send pmd (10) -> (11) forward pmd (12) -> (13) save pmd (14) -> end (15)
	// Assume all connectors in the system are reliable so all messages are received and processed according to components
	System.ReliableInternals[] and CC_Gateway_TOE.ReliableInternals[] => Pt_SMGW_ProcessAllMeterData
}
check SMGW_AC for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 6 Message, 15 steps
