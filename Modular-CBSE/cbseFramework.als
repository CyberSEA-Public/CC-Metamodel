module cbseFramework

/*
// A framework for defining a systems architecture made up of Components that define Interfaces
// on Ports of components, and the Connectors that connect the interfaces. It offers the ability
// to describe the communication between components using Messages and properties different
// entities have or are supposed to have.
*/

// Types and Signatures should have a Capital except for interfaces, operations, and parameters.
// They should be preceeded by their beginning lowercase letter ex. Interface -> i_<interface name>

// System Model Entities ========================================================================
// Model will be made up of abstract component-port-connector signatures. Actual models will extend signatures to construct their system
// Will allow division of system model into subsystems for higher level verification.

// Component =================================================================================
abstract sig Component {
	parent: lone Component,
	uses: set Port
}

// Node
sig CompositeComponent extends Component {
	contains: some Component
}{
	uses = this.external_ports[]
}

// Root Node
one sig System in CompositeComponent {}{
	no parent
}

// Leaf
sig AtomicComponent extends Component {}{
	one parent // ComponentsBelongToOneCompositeComponentOnly
	some uses
}

//Facts ========================================================
// A CompositeComponent is the parent of its components
fact { all cc: CompositeComponent, c: cc.contains | c.parent = cc }

// All components are connected in tree
fact { Component in System.*contains }

// A component can not only be connected to itself
fact { all c: Component | c.uses.connectors[].components[] != c }

// Functions ===================================================
fun CompositeComponent.ports[] : set Port {
	{ p : Port  | p in this.contains.uses } 
}

// if port is attached to a connector attached to port of component at the same level or under, it is considered internal 
fun CompositeComponent.internal_ports[] : set Port {
	{ p : Port | p in this.ports[] and p.connectors[].components[] in this.^contains } 
} 

// if port is attached to a connector attached to port of component at a higher level (not the same parent or sisters), it is considered external 
fun CompositeComponent.external_ports[] : set Port {
	{ p : Port | p in this.ports[] and p.connectors[].components[] not in this.^contains } 
}

// Get the InPorts of the component
fun Component.input[]: set InPort {
	InPort & this.uses
}

// Get the OutPorts of the component
fun Component.output[]: set OutPort {
	OutPort & this.uses
}

fun Component.outOperationPort[o: Operation]: one OutPort {
	{ p: OutPort | p in this.output[] and o in p.realizes.operations }
}

fun Component.inOperationPort[o: Operation]: one InPort {
	{ p: InPort | p in this.input[] and o in p.realizes.operations }
}

// Ensure reliable connectors in composite component when verifying properties of the components within will composite (in)variants
pred CompositeComponent.ReliableInternals[] {
	always all rc: this.InternalConnectors[] | all m:Message {
		(m in rc.buffer => eventually m.receivedOn[]) and // Message in buffer shall be received
		(m in (rc.connects & OutPort).sent => after m in rc.buffer) // Connector will not drop
	}
}
fun CompositeComponent.InternalConnectors[]: set Connector {
	let internal_p = this.internal_ports[] {
		{ c:Connector | all p:c.connects | p in internal_p }
	}
}

/* Communication primitives for operations*/
// A Component (this) sends an operation call to a Component c
pred AtomicComponent.send[c:Component,o:Operation] {
	this.outOperationPort[o].send[c.inOperationPort[o]]
	some m:OperationCall {
		m.operation = o
	}
}

// A Component (this) receive a operation call from a Component c
pred AtomicComponent.receive[o: Operation] {
	this.inOperationPort[o].receive[]
}

// Send operation call to a call and receive it
pred AtomicComponent.sendreceive[c: AtomicComponent, o: Operation] {
	this.send[c,o];
	c.receive[o]
}

pred sendOp[c1,c2:Component, o:Operation] {
	c1.send[c2,o]
}

pred sendreceiveTest[disj c1,c2:AtomicComponent, o:Operation] {
	c1.send[c2,o];
	c2.receive[o];
	c2.sendreceive[c1,o]
}

run sendOp for 3
run sendreceiveTest for 4 // Need at least 4 ports for bidirectional communication

// End Component =============================================================================

// Port ======================================================================================
abstract sig Port {
	realizes: set Interface
}
sig InPort extends Port {
	var received: set Message
}
sig OutPort extends Port {
	var sent: set Message
}

//Facts ========================================================
// All ports in the system must be connected to one Atomic Component only
fact AllPortsMustBeUsedByOneAtomicComponent {
	all p: Port | one p.~(AtomicComponent <: uses)
}

// Some connector should be connected to every port
fact PortsConnected {
	all p: Port | some p.~connects
}

// Port should realize at least one interface
fact PortRealized {
	all p: Port | some p.realizes
}

fact InPortReceivedFromBufferOnly {
	// Only receive from its connectors
	always all ip:InPort | ip.received in ip.connectors[].buffer
}

// A Port can only send or receive one message at a time

// Functions ===================================================
// return all connectors attached to the port
fun Port.connectors[]: set Connector {
	{ c: Connector | c in this.~connects }
}

fun Port.user[]: one Component {
	this.~uses
}

// Get the connectors that are shared between two components
fun sharedConnector[p1:OutPort,p2:InPort]: set Connector {
	p1.connectors[] & p2.connectors[]
}

// Port Messaging Predicates
pred OutPort.send[ip:InPort] {
	some m:Message {
		m.from = this
		m.to = ip
		m in this.sent
	}
}

pred OutPort.multiSend[ips: set InPort] {
	all ip:ips | this.send[ip]
}

pred InPort.receive[] {
	some this.connectors[].buffer
	some c:this.connectors[] | some m: c.buffer {
		m.to = this
		m in this.received
	}
}

pred send_message(c1,c2:Component) {
	some op: c1.output[], ip: c2.input[] {
		op.send[ip]
	}
	after after eventually some received
}

pred sendrcv_message(c1,c2:Component) {
	no Message;
	eventually some received
}

// Send a message to two components in one message
// Instead each broadcast message should have its own unique message. A pred can be used to divide the broadcast messages
pred send_message2(o:OutPort, disj i1,i2:InPort) {
	one m:Message {
		m.from = o
		m.to = (i1 + i2)
		m in o.sent
	}
}

pred send_message2multi(o:OutPort, disj i1,i2:InPort) {
	o.multiSend[i1+i2]
}

run send_message for 3 Component, 2 Connector, 4 Port, 2 Message, 2 Interface, exactly 3 Operation, 5 Parameter, 5 Variable, 0 Constant
run sendrcv_message for 3 Component, 1 Connector, 2 Port, 1 Message, 1 Interface, 3 Operation, 0 Parameter, 0 Variable, 0 Constant
run send_message2multi for 3 Component, 1 Connector, 3 Port, 2 Message, 1 Interface, 3 Operation, 0 Parameter, 0 Variable,  0 Constant
//run send_message2 for 3 Component, 1 Connector, 3 Port, 1 Message, 1 Interface, 3 Operation, 0 Parameter, 0 Variable, 0 Constant
// End Port ==================================================================================

// Connector =================================================================================
// Connects ports of components and passes messages between them (Unreliable and out-of-order possible)
sig Connector {
	connects: some Port,
	var buffer: set Message
}

// One way channel that connects the In and Out Ports of two components
sig Channel extends Connector {}{
	one (connects & InPort) and one (connects & OutPort) and #this.components[] = 2
}

// Two way channel between two components, where each component has one InPort and one OutPort
sig BiChannel extends Connector {}{
	#(InPort & connects) = 2 and #(OutPort & connects) = 2 and #this.components[] = 2
	#(connects & InPort).~uses = 2 and #(connects & OutPort).~uses = 2
}

// Facts =======================================================
// All connectors must at least connect one InPort and one OutPort
fact ConnectorHasAtLeastOneInAndOutPort {
	all c: Connector | some (InPort & c.connects) and some (OutPort & c.connects)
}

// Functions ===================================================
// return all atomic components attached to the connector
fun Connector.components[]: set Component {
	{ c: AtomicComponent | some c.uses & this.connects} // Connectors connect atomic components, non atomic components are always true
}

// Message is received from connector and not longer in its buffer
pred Connector.receive[m:Message] {
	after always m not in this.buffer
}

// The connector drops the message in its buffer
pred Connector.drop[m:Message] {
	always m not in this.buffer
}

// End Connector =============================================================================

// Interface =================================================================================
abstract sig Interface {
	operations: set Operation
}

abstract sig Operation {
	params: set Parameter
}

// Specify parameter sig so constraints and conditions can be placed on the parameter
abstract sig Parameter {
	var passes: set (Variable + Constant) // passes represents the values that can be passed to this parameter (domain)
}{
	// passes should be a restricted set of values which the parameter can be
} // Facts can be appended to a Parameter to constraint the Variable types. Facts should also be appended to
  // control what subset of the typed parameter values should be able to be passed as an argument

// Abstract representation of any data type
abstract var sig Variable {} // Variable value that can be created, modified, or removed

abstract sig Constant {} // A constant value that is available through all steps


// Facts =======================================================
fact OperationBelongsToInterface {
	all o: Operation | some o.~operations
}

fact ParameterBelongsToOperation {
	all p: Parameter | some p.~params
}

fact InterfaceHasOperation {
//	all i: Interface | some i.operations
}

// An Interface is used by an InPort and an OutPort
fact { all i: Interface |  some (i.ports[] & InPort) and some (i.ports[] & OutPort) }

// Functions ===================================================
fun Interface.ports[]: set Port {
	{ p : Port | p in this.~realizes }
}

// End Interface =============================================================================

// Message =======================================================================================
var abstract sig Message {
	var from: one OutPort,
	var to: one InPort
}

var abstract sig OperationCall extends Message {
	var operation: one Operation,
	var arguments: Parameter -> (Variable + Constant)
}{
	always no arguments[Parameter - operation.params] // arguments will only contain relations of the operation's parameters
	
	// One argument for every parameter of call's operation and the argument is a value in parameter's passable values
	// Parameter -> some (Variable + Constant)
	always all p: operation.params | one arguments[p] and arguments[p] in p.passes

	// The operation should be part of the senders out port interface
	always operation in from.realizes.operations

	// The operation should be part of the receivers in port interface and the operation call can only be sent to a port that realizes the operation
	always operation in to.realizes.operations

	always {
		// Ensure operation and arguments remain unchanged until deletion of message
		(operation' = operation and arguments' = arguments) or this.release[]
	}
}

//var sig EmptyMessage extends Message {}

// Facts =======================================================

fact toFromStatic {
	always all m: Message | (m.to' = m.to and m.from' = m.from) or m.release[]
}

pred Message.release[] {
	after always this not in Message
}

fact RemoveReceivedMessage {
	always all m: Message | m.receivedOn[] and after no buffer.m => m.release[] // Remove message after being received
}

fact ReleaseMessageCompletelyDropped {
	always all m: Message | m.sentOn[] and after no buffer.m => m.release[]
}

fact MessageCreatedOnSend {
	always all m:Message | (m not in Message until m.sentOn) or once m.sentOn
}

// All messages originate from their origin
fact MessageOriginCorrect {
	always all m:Message | m.sentOn => m.sentBy[] = m.from
}

fact UniqueSendInstant {
	// all message are sent exactly once
	all m:Message | eventually m.sentOn
	always {
		all m:Message { 
			(m.sentOn => after always no sent.m)
			(lone m.sentBy[])
		}
	}
}

fact MessageNotBufferBeforeSend {
	always all m: Message | (m not in Connector.buffer until m.sentOn) or once m.sentOn
}

// A message shouldn't be sent to a buffer which the message to and from components do not share
fact MessageNotInNonSharedBuffer {
	always all m: Message | m not in (Connector - sharedConnector[m.from,m.to]).buffer
}

fact MessageInBufferAfterSend {
	always {
		// all messages should be sent to all shared connectors // How about if we want to choose which connector we send to?
		all m:Message | all sb:sharedConnector[m.from,m.to] | m.sentOn[] => m not in sb.buffer and after (m in sb.buffer or sb.drop[m])
	}
}

/*fact noDrop {
	always all m:Message | m.sentOn[] => after some buffer.m
}*/

fact InBufferUntilReceived {
	always all m: Message | all sb:sharedConnector[m.from,m.to] | m in sb.buffer => ((sb.receive[m] and m.receivedOn[]) releases m in sb.buffer)
}

fact EmptyBufferAfterReceived {
	always all m:Message | m.receivedOn[] => some c:Connector | c in buffer.m and c.receive[m]
}

fact UniqueReceivedInstant {
	// all message are received exactly once
	always {
		all m:Message {
			(lone m.receivedBy[])
		}
	}
}

// Message should be only received by the comp in the to field of the message
fact MessageReceivedByTo {
	always all m:Message | m.receivedOn[] => m.to = m.receivedBy[]
}

fact ReceivedMustBeValid {
	always all m:Message | m.receivedOn[] => m.receivedBy[].receive[]
}

// Functions ===================================================
// Helper Message Predicates and Functions
pred sentOn[m:Message] {
	some sent.m
}

fun Message.sentBy[]: set OutPort {
	sent.this
}

pred Message.receivedOn[] {
	some received.this
}

fun Message.receivedBy[]: set InPort {
	received.this
}

/*fun OperationCall.getArg[p:Parameter]: some (Variable + Constant) {
	p.passes & this.arguments
}*/

// End Message ===================================================================================

// End Entities ==============================================================================

// Some facts that may exist but not for this model
/*
1. Every component port must be connected to a connector - this wasn't stated as a fact but an assertion for a complete model (reverted)
2. Connectors can connect more than one InPort and OutPort to model networks and other systems in which more than two components
   communication through the same connector. The connector's interaction nature and components' ports should be
   able to handle the communication through their rules and interfaces.
3. A component doesn't have to provide a service to the system's perspective because internally it could provide a service to it's
   environment based on a required service from the system. A component must be connected to the system in some way though, so
   a component must have at least one InPort or one OutPort
*/

// Testing System Model (Not always valid) =====
pred ex {}
// run ex for exactly 1 System, exactly 2 CompositeComponent, exactly 5 Component, exactly 10 Port, exactly 4 Connector
run ex for 10 but exactly 4 CompositeComponent, exactly 6 AtomicComponent, exactly 8 Port

MessageInTwoBuffer: run {
	eventually one m:Message | #m.~buffer > 1 and after m.receivedOn
} for 3

MessageReceivedFromTwoBuffer: run {
	eventually one m:Message {
		#m.~buffer > 1 and m.receivedOn;
		m.receivedOn
	}
} for 3

run someDelete {
	some sent;
	some received;
	eventually no Message
} for 4

run someDrop {
	some sent;
	no Message
} for 4
DualDestinationRun: run {
	eventually some m:Message | #m.to > 1
} for 4

//run ex for exactly 1 System, 4 Component, 2 Connector, exactly 9 Port, 4 Message, 4 Interface, 8 Operation, 0 Parameter, 0 Variable, 0 Constant
// run ex for exactly 1 System, exactly 3 CompositeComponent, exactly 4 AtomicComponent, exactly 4 Port, exactly 4 Connector
// run ex for exactly 1 System, exactly 4 CompositeComponent, exactly 4 AtomicComponent, exactly 4 Port, exactly 3 Connector
// run ex for exactly 1 System, 1 CompositeComponent, 2 AtomicComponent, 4 Port, 1 Connector, exactly 1 BiChannel, 4 Interface, 4 Operation, 4 Parameter, 4 Variable, 4 Message

// Assertions ================================================================================
// Safety assertion for Component's distinct (disj) uses relation // Now uses 'one this.~uses' since it also ensures each port is used by
// one component only
PortOnlyUsedOnce: check {
	all disj c1,c2: AtomicComponent | all p: Port | p in c1.uses => p not in c2.uses
} for 5

// All components and connectors are attached. No hanging ports
ConnectedSystem: check {
	all p: Port | some c: Connector | p in c.connects
} for 5

// Verification Composition Component ===========
// The composition tree contains path which are acyclic
CompositionIsAcyclic: check { no cc: CompositeComponent | cc in cc.^contains } for 5

// System has one root
CompositionOneRoot: check  { one cc: CompositeComponent | no cc.parent } for 5

// Every Component is in at most one CompositeComponent
CompositionAtomAreOneLocation: check { all c: Component | lone cc: CompositeComponent | c in cc.contains } for 5

// Ports is either internal or external
CompositeComponentPortIsEitherInternalOrExternal: check { all cc: CompositeComponent | cc.ports[] = cc.internal_ports[] + cc.external_ports[] } for 5

// The internal and external ports of a composite component are disjointed sets
DisjointInternalAndExternal: check { all cc: CompositeComponent |  (cc.internal_ports[] & cc.external_ports[]) = none } for 5

// The Root system node should have no external ports
SystemHasNoExternalPorts: check { no System.uses } for 5


// Messageing Checks
check AlwaysReceived { // Should be able to drop messages?
	eventually all m: Message | m.receivedOn
} for 5//2 Comp, 1 Con, 1 Message

// A message should not be in a buffer before it is sent
check MessageNotInBufferBeforeSend {
	always all m:Message | m.sentOn[] => historically no m.~buffer
} for 5

// If a message is in a buffer then it was sent
check MessageInBufferSent {
	always all m: Message | some m.~buffer => once m.sentOn[]
} for 5

check AllSentMessageInBufferOrDropped {
	always all m:Message | m.sentOn[] => after (some buffer.m or Connector.drop[m])
} for 5

// A message should only be created on the sending of the message
check AllMessageCreatedSent {
	always all m:Message | once m.sentOn[]
} for 5

// Message should be cleared after being received from a buffer
check BufferEmptyedOnReceived {
	always all m: Message | m.receivedOn[] => #m.~buffer > #m.~(buffer')
} for 5//2 Comp, 1 Con, 1 Message

// If a message was received then it was sent
check AllMessageReceivedWereSent {
	always all m:Message | m.receivedOn[] => once m.sentOn[]
} for 5//2 Comp, 1 Con, 1 Message

check AllReceivedMessagesFromBuffer {
	always all m:Message | m.receivedOn[] => once some buffer.m
} for 5

check AllMessageReceivedRemoved {
	always all m:Message | m.receivedOn[] and no buffer.m => after always m not in Message
} for 5

// A message's from field is always from its sender
check FromIsSender {
	always all m:Message | some m.~sent => m.from = m.sentBy[]
} for 5//2 Comp, 1 Con, 1 Message

check BufferStaysEmptyIfMessageDropped {
	always all m:Message, c:Connector | once m.sentOn[] and after m not in c.buffer => m in c.buffer or always m not in c.buffer
} for 5

check MessagesLeavingBufferAreReceived {
	always all m:Message | once some buffer.m and no buffer.m => once m.receivedOn[]
} for 5

// Operation call checks
check OpCallOnlyUsingOperationsOnPort {
	always all o:OperationCall | o.operation in o.from.realizes.operations
} for 5

check OpCallOnlySentToPortWithOperation {
	always all o:OperationCall | o.receivedOn[] => o.operation in o.receivedBy[].realizes.operations
} for 5

check MessageDeleted {
	always all m:Message | m.receivedOn => eventually m not in Message
} for 5

// By default messages can be received out of order, or at the same time, because they can be received out of order
// Can be changed by creating a fact like the one below or on the behaviour of the connector
/*fact ReceiveRightAway {
	always all m:Message | m.sentOn[] => after m.receivedOn[]
}*/
check ReceivingOrder {
	always all disj m1,m2:Message {
		once (m1.sentOn[] and after m2.sentOn[]) and eventually m1.receivedOn[] and eventually m2.receivedOn[] => 
			m1.receivedOn[] and after m2.receivedOn[]
	}
} for 5
